# guia 3-1 aed

Guia 3_1 Algoritmos y estructura de datos.

Benjamin Martin Albornoz.

En el programa del ejercicio1 se escribe un codigo que crea una lista de numeros desordenada y luego la separa en dos listas diferentes e independientes que estan ordenadas ascendentemente, una para los numeros positivos y otra para los negativos, y luego imprime el contenido de cada lista.

En el programa del ejercicio2 se escribe un codigo que crea una lista ordenada de nombres (tipo string) y que por cada ingreso de estos nombres, muestra el contenido actual de la lista.

Para compilar los programas del ejercicio 1, ejecutar make en terminal y luego el comando ./Main1
Para compilar los programas del ejercicio 2, ejecutar make en terminal y luego el comando ./Main2
