#ifndef LISTANOM_H
#define LISTANOM_H
#include <iostream>
using namespace std;

// Se crea la estructura nodo.
typedef struct nodo{
    string nom;
    struct nodo *prox;
} nodo;

class Listanom{
    private:
    nodo *first = NULL;
    
    public:
    // Constructor de la clase.
    Listanom();

    // Funcion que crea la lista de nombres .
    void crear(string nom);

    // Funcion que imprime los elementos de la lista.
    void imprimir();
};

#endif