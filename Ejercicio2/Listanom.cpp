
#include <iostream>
#include "Listanom.h"
using namespace std;

Listanom::Listanom(){

}

// Funcion que crea una lista con elementos de tipo string (nombres).
void Listanom::crear(string nom){
    nodo *temp1;
    temp1 = new nodo;
    temp1 -> nom = nom;
    temp1 -> prox = NULL;
    char nombre = toupper(nom[0]);

    if(this -> first == NULL || nombre < toupper(this -> first -> nom[0])){
       temp1 -> prox = this -> first;
       this -> first = temp1;
    }else{
        nodo *temp2;
        temp2 = this -> first;
        while(temp2 -> prox != NULL && toupper(temp2 -> prox -> nom[0] < nombre)){
            temp2 = temp2 -> prox;
        }
        if(temp2 -> prox != NULL){
            temp1 -> prox = temp2 -> prox;
        }
        temp2 -> prox = temp1;
    }
    cout << "Se agrego un nombre: " << endl;
    cout << "---------------------" << endl;
    cout << "Lista actual: " << endl;
    imprimir();
}

// Funcion que imprime los elementos dentro de la lista
// utilizando un nodo temporal que recorra el ciclo while,
// imprimiendo los elementos por cada iteracion. 
void Listanom::imprimir(){
    nodo *temp1 = this -> first;
    while(temp1 != NULL){
        cout << "Nombre: " << temp1 -> nom << endl;
        temp1 = temp1 -> prox;
    }
}