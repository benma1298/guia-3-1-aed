
#include <iostream>
#include <ctype.h>
#include "Listanom.h"
using namespace std;

int main(){
    Listanom *listanom = new Listanom();
    int cantidad;
    string n;
    cout << "Ingrese la cantidad de nombres que tendra la lista: " << endl;
    cin >> cantidad;
    for(int i = 0; i < cantidad; i++){
        cin.ignore();
        cout << "Ingrese el nombre: ";
        getline(cin, n);
        listanom -> crear(n);
        cout << "PRESIONE ENTER" << endl;
    }

cout << "Lista : "<< endl;
listanom -> imprimir();

delete listanom;
return 0;
}