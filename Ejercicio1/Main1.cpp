
#include <iostream>
#include <time.h>
#include "Listanum.h"
using namespace std;

int main(){
    // Funcion usada en el numero al azar.
    srand(time(NULL));
    int cantidad;
    int random;
    // Se crean las listas.
    Listanum *lista = new Listanum();
    Listanum *listapositivos = new Listanum();
    Listanum *listanegativos = new Listanum();

    cout << "Ingrese la cantidad de numeros que tendra la lista." << endl;
    cin >> cantidad;
    
    // Se define el numero al azar entre -10 y 10 mediante funcion rand.
    for(int i = 0; i < cantidad; i++){
        random = rand()%(21)-10;
        lista -> crear(random);
    }

    // Se imprimen los elementos de las 3 listas.
    cout << "Lista inicial: " << endl;
    lista -> imprimir();
    cout << endl;

    listapositivos -> dividir(1, lista);
    listanegativos -> dividir(0, lista);
    
    cout << "Lista Positivos: " << endl;
    listapositivos -> imprimir();
    cout << endl;

    cout << "Lista Negativos: " << endl;
    listanegativos -> imprimir();
    cout << endl;

    // Se libera memoria usada en las listas.
    delete lista;
    delete listapositivos;
    delete listanegativos;

    return 0;
}