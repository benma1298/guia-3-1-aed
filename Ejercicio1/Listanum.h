
#ifndef LISTANUM_H
#define LISTANUM_H
#include <iostream>
using namespace std;

// Se crea la estructura nodo.
typedef struct nodo{
    int num;
    struct nodo *prox;
} nodo;

class Listanum{
    private:
    nodo *first = NULL;
    nodo *last = NULL;

    public:
    // Constructor de clase.
    Listanum();

    // Funcion que crea la lista.
    void crear(int num);

    // Funcion que ordena los elementos de una lista.
    void ordenar(int num);

    // Funcion que imprime los elementos de una lista.
    void imprimir();

    // Funcion que divide a la lista entre positivos (1) y negativos (0).
    void dividir(int numero, Listanum *l_inicio);
};

#endif