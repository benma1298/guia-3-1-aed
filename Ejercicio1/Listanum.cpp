
#include <iostream>
#include "Listanum.h"
using namespace std;

Listanum::Listanum(){
}

// Se crea una lista con sus elementos desordenados.
void Listanum::crear(int num){
    nodo *temp1;
    temp1 = new nodo;
    temp1 -> num = num;
    temp1 -> prox = NULL;

    if(this-> first == NULL){
        this -> first = temp1;
        this -> last = this -> first;
    }else{
        this -> last -> prox = temp1;
        this -> last = temp1;
    }
}

// Se crea una lista con sus elementos ordenados, evaluando
// que el dato que ingrese sea menor que el primer elemento.
void Listanum::ordenar(int num){
    nodo *temp2;
    temp2 = new nodo;
    temp2 -> num = num;
    temp2 -> prox = NULL;

    if(this -> first == NULL || num < this -> first -> num){
        temp2 -> prox = this -> first;
        this -> first = temp2;
    }else{
        nodo *temp3;
        temp3 = this -> first;

        while(temp3 -> prox != NULL && temp3 -> prox -> num < num){
            temp3 = temp3 -> prox;
        }
        if(temp3 -> prox != NULL){
            temp2 -> prox = temp3 -> prox;
        }
        temp3 -> prox = temp2;
    }
}

// Funcion que imprime una lista a partir de la creacion de un nodo
// temporal tomando el valor del primer elemento de la lista (temp4);
// Este nodo imprimira la lista dentro de un ciclo while.
void Listanum::imprimir(){
    nodo *temp4 = this -> first;

    while(temp4 != NULL){
        cout << "Numero: " << temp4 -> num << endl;
        temp4 = temp4 -> prox;
    }
}

// Funcion que divide la lista en dos, una de elementos positivos y otra
// de elementos negativos, usando un nodo temporal (temp2) dentro de un
// ciclo while que recorra la lista determinando los positivos y negativos.
void Listanum::dividir(int numero, Listanum *l_inicio){
    nodo *temp2 = l_inicio -> first;

    while(temp2 !=NULL){
        if(numero == 1){
            if(temp2 -> num >= 0){
                crear(temp2 -> num);
            }
        }else if(numero == 0){
            if(temp2 -> num < 0){
                crear(temp2 -> num);
            }
        }
        temp2 = temp2 -> prox;
    }
}